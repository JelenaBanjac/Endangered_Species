﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using EndangeredSpecies.Domain;

namespace EndangeredSpecies
{
    /// <summary>
    /// 
    /// </summary>
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class JavaScriptControlHelper
    {
        MainWindow winMW;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        public JavaScriptControlHelper(MainWindow w)
        {
            winMW = w;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void RunFromJavascript(string param)
        {
            winMW.doThings(param);
        }


        Home winH;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        public JavaScriptControlHelper(Home w)
        {
            winH = w;
        }

        Tables winT;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        public JavaScriptControlHelper(Tables w)
        {
            winT = w;
        }

        Map winM;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        public JavaScriptControlHelper(Map w)
        {
            winM = w;
        }

        SpeciesDialog winSD;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        public JavaScriptControlHelper(SpeciesDialog w)
        {
            winSD = w;
        }

    }
}
