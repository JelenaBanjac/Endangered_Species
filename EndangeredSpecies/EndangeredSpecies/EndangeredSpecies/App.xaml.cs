﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for <c>App.xaml</c>
    /// </summary>
    /// <remarks>
    ///     <para>The <c>App</c> class manages application resources (resources for application appearance)
    ///     and references program to <c>MainWindow</c>.</para>
    /// </remarks>
    public partial class App : Application
    {
    }
}
