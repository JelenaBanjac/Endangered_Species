﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace EndangeredSpecies.Classes
{
    /// <summary>
    ///     <c>Species</c> are animals.
    /// </summary>
    public sealed class Species
    {
        /// <summary>
        ///     Types of endangered statuses of species.
        /// </summary>
        public static ObservableCollection<string> EndageredStatuses = new ObservableCollection<string>(new string[] { "Critically endangered", "Endangered", "Vulnerable",
            "Dependent on habitats preservation", "Close risk", "Least risk" });

        /// <summary>
        ///     Types of tourist statuses of species
        /// </summary>
        public static ObservableCollection<string> TouristStatuses = new ObservableCollection<string>(new string[] { "Isolated", "Partially Habitated", "Habitated" });

        /// <summary>
        ///     Initial constructor
        /// </summary>
        public Species()
        {
            TagsOfSpecies = new ObservableCollection<Tag>();
            Opacity = "0.001";
        }

        /// <summary>
        ///     Default constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        /// <param name="endangeredStatus"></param>
        /// <param name="image"></param>
        /// <param name="isDangerous"></param>
        /// <param name="isOnIUCNRedList"></param>
        /// <param name="livesInPopulatedRegion"></param>
        /// <param name="touristStatus"></param>
        /// <param name="discoveryDate"></param>
        /// <param name="annualTourismIncome"></param>
        /// <param name="tagsOfSpecies"></param>
        /// <param name="opacity"></param>
        public Species(string id, string name, string description, SpeciesType type, string endangeredStatus, string image, bool isDangerous,
            bool isOnIUCNRedList, bool livesInPopulatedRegion, string touristStatus, DateTime discoveryDate, double annualTourismIncome, 
            ObservableCollection<Tag> tagsOfSpecies, String opacity)
        {
            Id = id;
            Name = name;
            Description = description;
            Type = type;
            EndangeredStatus = endangeredStatus;
            Image = image;
            IsDangerous = isDangerous;
            IsOnIUCNRedList = isOnIUCNRedList;
            LivesInPopulatedRegion = livesInPopulatedRegion;
            TouristStatus = touristStatus;
            DiscoveryDate = discoveryDate;
            AnnualTourismIncome = annualTourismIncome;
            TagsOfSpecies = tagsOfSpecies;
            Opacity = opacity;
        }

        /// <summary>
        ///     Gets or sets the ID of species
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the Name of species
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the Description of species
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets the Type of species
        /// </summary>
        public SpeciesType Type { get; set; }

        /// <summary>
        ///     Gets or sets the endangered status of species
        /// </summary>
        public string EndangeredStatus { get; set; }

        /// <summary>
        ///     Gets or sets the image of species
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        ///      Gets or sets the danger status of species
        /// </summary>
        public bool IsDangerous { get; set; }

        /// <summary>
        ///     Gets or sets is the species on IUCN Red List of threatened species
        /// </summary>
        public bool IsOnIUCNRedList { get; set; }

        /// <summary>
        ///     Gets or sets value of does species lives in populated region
        /// </summary>
        public bool LivesInPopulatedRegion { get; set; }

        /// <summary>
        ///     Gets or sets tourist status of species
        /// </summary>
        public string TouristStatus { get; set; }

        /// <summary>
        ///     Gets or sets discovery date of species
        /// </summary>
        public DateTime DiscoveryDate { get; set; }

        /// <summary>
        ///     Gets or sets annual toursm income of species
        /// </summary>
        public double AnnualTourismIncome { get; set; }

        /// <summary>
        ///     Gets or sets tags of species
        /// </summary>
        public ObservableCollection<Tag> TagsOfSpecies { get; set; }

        /// <summary>
        ///     Gets or sets opacity of species for map represenation
        /// </summary>
        public string Opacity { get; set; }

    }
}

