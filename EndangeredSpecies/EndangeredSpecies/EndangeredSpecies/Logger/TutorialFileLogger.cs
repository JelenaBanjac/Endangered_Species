﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EndangeredSpecies.Logger
{
    /// <summary>
    ///     TutorialFileLogger comunicates with text files that contain status of tutorial
    /// </summary>
    public sealed class TutorialFileLogger
    {
        /// <summary>
        ///      Write inside the file to save status of tutorial
        /// </summary>
        public static void WriteInFile()
        {
            string line = "";

            if (DataLists.tutorialOn == true)
            {
                line = "true";
            } else
            {
                line = "false";
            }

            System.IO.File.WriteAllText("Tutorial.txt", line);
           
        }

        /// <summary>
        ///     Reads status of the tutorial
        /// </summary>
        /// <returns></returns>
        public static bool ReadFile()
        {
            bool tutorialOn = false;
            
            try
            {
                if (!System.IO.File.Exists("Tutorial.txt"))
                    throw new System.IO.FileNotFoundException();

                string line = System.IO.File.ReadAllText("Tutorial.txt");

                if (line.Equals("true"))
                {
                    tutorialOn = true;
                } else
                {
                    tutorialOn = false;
                }


            } catch (System.IO.FileNotFoundException e)
            {
                // your message here.
            }
            return tutorialOn;

        }
    }
}
