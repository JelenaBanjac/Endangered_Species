﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EndangeredSpecies.Classes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace EndangeredSpecies.Logger
{
    /// <summary>
    ///     DAataLists has all the lists that comunicate with data in text files.
    /// </summary>
    public sealed class DataLists
    {
        /// <summary>
        ///     Remembers if user is in tutorial mode
        /// </summary>
        public static bool tutorialOn = false;

        /// <summary>
        ///     List of all tags in system
        /// </summary>
        public static ObservableCollection<Tag> tagList = new ObservableCollection<Tag>();

        /// <summary>
        ///     List of all the species in system
        /// </summary>
        public static ObservableCollection<Species> speciesList = new ObservableCollection<Species>();

        /// <summary>
        ///     List of all the species types in system
        /// </summary>
        public static ObservableCollection<SpeciesType> speciesTypeList = new ObservableCollection<SpeciesType>();

        /// <summary>
        ///     Constructor for DataLists that reads data from files
        /// </summary>
        public DataLists()
        {
            tutorialOn = TutorialFileLogger.ReadFile();
            tagList = TagFileLogger.ReadFile();
            speciesList = SpeciesFileLogger.ReadFile();
            speciesTypeList = SpeciesTypeFileLogger.ReadFile();
            CollectDataTag();
            CollectDataSpecies();
            CollectDataSpeciesType();           
        }

        /// <summary>
        ///     In case reader cannot read the file, here is backup data for tags
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<Tag> CollectDataTag()
        {
            return new ObservableCollection<Tag>
            {
                new Tag
                (
                    "11",
                    "Yellow",
                    "Yellow is for medium endangered status"
                ),
                new Tag
                (
                    "22",
                    "Green",
                    "Green is for not endangered status"
                ),
                new Tag
                (
                    "33",
                    "Red",
                    "Red is for very endangered status"
                )
            };
        }

        /// <summary>
        ///     In case reader cannot read the file, here is backup data for species types
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<SpeciesType> CollectDataSpeciesType()
        {
            return new ObservableCollection<SpeciesType>
            {
                new SpeciesType
                (
                    "1",
                    "Mamal",
                    "Mamals are interesting",
                    ""
                ),
                new SpeciesType
                (
                    "2",
                    "Sponge",
                    "Sponge are very interesting",
                    ""

                ),
                new SpeciesType
                (
                    "3",
                    "Arthropod",
                    "Arthropod are not interesting",
                    ""

                )
            };
        }

        /// <summary>
        ///     In case reader cannot read the file, here is backup data for species
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<Species> CollectDataSpecies()
        {
            String opacity = "0.2";

            return new ObservableCollection<Species>
            {
                new Species
                (
                    "123",
                    "Panda",
                    "Black and white animal",
                    FindTypeById("1"),
                    "Critically endangered",
                    "../Images/default.png",
                    false,
                    true,
                    true,
                    "Partially Habitated",
                    new DateTime(),
                    1000000,
                    new ObservableCollection<Tag>{ DataLists.FindTagById("1"), DataLists.FindTagById("2"), },
                    opacity

                ),
                new Species
                (
                    "432",
                    "Elephant",
                    "Gray animal",
                    FindTypeById("1"),
                    "Vulnerable",
                    "../Images/rsz_elephant.png",
                    false,
                    false,
                    true,
                    "Partially Habitated",
                    new DateTime(),
                    900000,
                    new ObservableCollection<Tag>{ DataLists.FindTagById("2") },
                    opacity


                ),
                new Species
                (
                    "586",
                    "Monkey",
                    "Brown animal",
                    FindTypeById("1"),
                    "Least risk",
                    "../Images/rsz_monkey.png",
                    false,
                    false,
                    true,
                    "Partially Habitated",
                    new DateTime(),
                    900000,
                    new ObservableCollection<Tag>{ DataLists.FindTagById("1"), DataLists.FindTagById("3") },
                    opacity


                )
            };
        }
       
        /// <summary>
        ///     Finds species by their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Species FindSpeciesById(String id)
        {
            Species species = new Species();

            foreach (var data in speciesList)
            {
                if (data.Id.Equals(id))
                {
                    return data;
                }
            }

            return species;
        }

        /// <summary>
        ///     Finds species type by their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SpeciesType FindTypeById(string id)
        {
            SpeciesType type = new SpeciesType();

            foreach (var data in speciesTypeList)
            {
                if (data.Id.Equals(id))
                {
                    return data;
                }
            }

            return type;
        }

        /// <summary>
        ///     Finds species types by their name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static SpeciesType FindTypeByName(string name)
        {
            SpeciesType type = new SpeciesType();

            foreach (var data in speciesTypeList)
            {
                if (data.Name.Equals(name))
                {
                    return data;
                }

            }         

            return type;
        }

        /// <summary>
        ///     Finds tag by their ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Tag FindTagById(String id)
        {
            Tag tag = new Tag();

            foreach (var data in tagList)
            {
                if (data.Id.Equals(id))
                {

                    return data;
                }
            }

            return tag;
        }
    }
}
