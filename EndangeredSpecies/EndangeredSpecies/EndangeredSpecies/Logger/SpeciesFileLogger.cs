﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EndangeredSpecies.Classes;
using System.Collections.ObjectModel;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EndangeredSpecies.Logger
{
    /// <summary>
    ///     SpeciesFileLogger comunicates with text files that contain all the species for system
    /// </summary>
    public sealed class SpeciesFileLogger
    {
        /// <summary>
        ///     Write inside the file of species
        /// </summary>
        public static void WriteInFile()
        {

            string[] lines = new string[DataLists.speciesList.Count];
            for (int i = 0; i < DataLists.speciesList.Count; i++)
            {
                String line = DataLists.speciesList[i].Id + "|" + DataLists.speciesList[i].Name + "|" +
                    DataLists.speciesList[i].Description + "|" + DataLists.speciesList[i].Type.Id + "|" +
                    DataLists.speciesList[i].EndangeredStatus + "|" + DataLists.speciesList[i].Image + "|" +
                    DataLists.speciesList[i].IsDangerous + "|" + DataLists.speciesList[i].IsOnIUCNRedList + "|" +
                    DataLists.speciesList[i].LivesInPopulatedRegion + "|" + DataLists.speciesList[i].TouristStatus + "|" +
                    DataLists.speciesList[i].DiscoveryDate + "|" + DataLists.speciesList[i].AnnualTourismIncome + "|";

                if (DataLists.speciesList[i].TagsOfSpecies.Count > 0)
                {
                    
                    int j = 0;
                    for (j = 0; j < DataLists.speciesList[i].TagsOfSpecies.Count - 1; j++)
                    {
                        line += DataLists.speciesList[i].TagsOfSpecies[j].Id + ",";
                    }
                    line += DataLists.speciesList[i].TagsOfSpecies[j].Id;
                }
           
                lines[i] = line;
            }

            System.IO.File.WriteAllLines("Species.txt", lines);
        }

        /// <summary>
        ///     Reads all the species from files that contain species
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<Species> ReadFile()
        {
            ObservableCollection<Species> list = new ObservableCollection<Species>();
            try
            {
                if (!System.IO.File.Exists("Species.txt"))
                    throw new System.IO.FileNotFoundException();
                string[] lines = System.IO.File.ReadAllLines("Species.txt");

                foreach (string line in lines)
                {
                    string[] words = line.Split('|');

                    Species species = new Species();
                    species.Opacity = "0.2";
                    species.Id = words[0];
                    species.Name = words[1];
                    species.Description = words[2];
                    species.Type = DataLists.FindTypeById(words[3]);
                    
                    species.EndangeredStatus = words[4];
                    species.Image = words[5];
                    species.IsDangerous = words[6].Equals("True") ? true : false;
                    species.IsOnIUCNRedList = words[7].Equals("True") ? true : false;
                    species.LivesInPopulatedRegion = words[8].Equals("True") ? true : false;
                    species.TouristStatus = words[9];
                    species.DiscoveryDate = Convert.ToDateTime(words[10]);

                    double annualIncome = 0;
                    if (!double.TryParse(words[11], out annualIncome))
                        species.AnnualTourismIncome = annualIncome;
                    species.AnnualTourismIncome = annualIncome;

                    String tagIdsString = words[12];
                    string[] tagIdsList = tagIdsString.Split(',');
                    
                    foreach (var tagId in tagIdsList)
                    {
                        species.TagsOfSpecies.Add(DataLists.FindTagById(tagId));
                    }

                    list.Add(species);

                }
                return list;
            }
            catch (System.IO.FileNotFoundException e)
            {
                // your message here.
            }
            list = DataLists.CollectDataSpecies();
            return list;
        }

    }
}
