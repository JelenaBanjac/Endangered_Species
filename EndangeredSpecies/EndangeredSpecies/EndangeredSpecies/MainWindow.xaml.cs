﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EndangeredSpecies.Domain;
using EndangeredSpecies.Logger;
using MaterialDesignThemes.Wpf;


namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    ///     MainWindow is the core window of the application
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        ///     <c>MainWindow</c> is the core window of the application. 
        ///     It has side menu where user can choose which window will be currently active.
        /// </summary>
        public MainWindow()
        {
            DataLists dl = new DataLists();
            InitializeComponent();

            
        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Tables.prepareForTutorial();
            Home.prepareForTutorial();
            Map.prepareForTutorial();
            MenuToggleButton.IsChecked = false;
        }

        private async void AddSpieces_PopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var sampleMessageDialog = new SpeciesDialog
            {
                //Message = { Text = ((ButtonBase)sender).Content.ToString() }
            };

            await DialogHost.Show(sampleMessageDialog, "RootDialog");
        }

        private async void AddSpiecesType_PopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var sampleMessageDialog = new SpeciesTypeDialog
            {
                //Message = { Text = ((ButtonBase)sender).Content.ToString() }
            };

            await DialogHost.Show(sampleMessageDialog, "RootDialog");
        }

        private async void AddTag_PopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var sampleMessageDialog = new TagDialog
            {
                //Message = { Text = ((ButtonBase)sender).Content.ToString() }
            };

            await DialogHost.Show(sampleMessageDialog, "RootDialog");
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void doThings(string param)
        {
            //btnOK.Background = new SolidColorBrush(Color.FromRgb(32, 64, 128));
            Title = param;
        }

    }
}
