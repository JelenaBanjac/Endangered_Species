﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using EndangeredSpecies.Domain;
using MaterialDesignThemes.Wpf;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.ComponentModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;
//using MaterialDesignColors.WpfExample.Domain;


namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for <c>Tables.xaml</c>
    /// </summary>
    /// <remarks>
    ///     <para>Class <c>Tables</c> is used visual representation of <c>Species</c>.
    ///     This class handles openings of dialogs like <b>ADD</b>, <b>DETAILS</b>, <b>DELETE</b> and <b>EDIT (UPDATE)</b> Species.
    ///     Also, it handles FILTER dialog and searches for species with a certain ID or Name.
    ///     </para>
    /// </remarks>
    public partial class Tables : UserControl
    {
        FilteredSearch newFiltededSearch = null;
        private List<SelectableViewModel> selectedSpecies = new List<SelectableViewModel>();

        /// <summary>
        ///     <c>Tables</c> is used visual representation of <c>Species</c>.     
        /// </summary>
        public Tables()
        {
            InitializeComponent();

            DataLists dl = new DataLists();

            ProgressBar.Visibility = Visibility.Hidden;
            
            prepareForTutorial();
        }

        /// <summary>
        ///     Prepare Map window in case user is in tutorial mode.
        /// </summary>
        public void prepareForTutorial()
        { 
            if (DataLists.tutorialOn == true)
            {
                Canvas.SetZIndex(Tutorial, 12);
                Canvas.SetZIndex(FoggyRect, 10);

                Canvas.SetZIndex(TableDialog, 9);
                Canvas.SetZIndex(Tutorial0, 9);
                Canvas.SetZIndex(Tutorial1, 9);
                Canvas.SetZIndex(Tutorial2, 9);
                Canvas.SetZIndex(Tutorial3, 9);
                Canvas.SetZIndex(Tutorial4, 9);
                Canvas.SetZIndex(Tutorial5, 9);
                Canvas.SetZIndex(TutorialFinish, 9);
                /*Canvas.SetZIndex(Tutorial6, 9);*/

                Canvas.SetZIndex(BackgroundRect0, 9);
                Canvas.SetZIndex(BackgroundRect1, 9);
                Canvas.SetZIndex(BackgroundRect2, 9);
                Canvas.SetZIndex(BackgroundRect3, 9);
                Canvas.SetZIndex(BackgroundRect4, 9);
                Canvas.SetZIndex(BackgroundRect5, 9);
                /*Canvas.SetZIndex(BackgroundRect6, 9);*/
                // Ostali slojevi
               
                FoggyRect.Visibility = Visibility.Visible;
                FoggyRect.Opacity = 0.5;

                BackgroundRect0.Visibility = Visibility.Hidden;
                BackgroundRect1.Visibility = Visibility.Hidden;
                BackgroundRect2.Visibility = Visibility.Hidden;
                BackgroundRect3.Visibility = Visibility.Hidden;
                BackgroundRect4.Visibility = Visibility.Hidden;
                BackgroundRect5.Visibility = Visibility.Hidden;
                /*BackgroundRect6.Visibility = Visibility.Hidden;*/

                //TutorialHomeDialog.Visibility = Visibility.Hidden;
                Tutorial.Visibility = Visibility.Visible;
                Tutorial0.Visibility = Visibility.Hidden;
                Tutorial1.Visibility = Visibility.Hidden;
                Tutorial2.Visibility = Visibility.Hidden;
                Tutorial3.Visibility = Visibility.Hidden;
                Tutorial4.Visibility = Visibility.Hidden;
                Tutorial5.Visibility = Visibility.Hidden;
                TutorialFinish.Visibility = Visibility.Hidden;
                /*Tutorial6.Visibility = Visibility.Hidden;*/

            }
            else
            {
                Canvas.SetZIndex(TableDialog, 12);
                Canvas.SetZIndex(FoggyRect, 9);
                Canvas.SetZIndex(BackgroundRect0, 9);
                Canvas.SetZIndex(BackgroundRect1, 9);
                Canvas.SetZIndex(BackgroundRect2, 9);
                Canvas.SetZIndex(BackgroundRect3, 9);
                Canvas.SetZIndex(BackgroundRect4, 9);
                Canvas.SetZIndex(BackgroundRect5, 9);
                /*Canvas.SetZIndex(BackgroundRect6, 9);*/
                // Ostali slojevi
                Canvas.SetZIndex(Tutorial, 9);
                Canvas.SetZIndex(Tutorial0, 9);
                Canvas.SetZIndex(Tutorial1, 9);
                Canvas.SetZIndex(Tutorial2, 9);
                Canvas.SetZIndex(Tutorial3, 9);
                Canvas.SetZIndex(Tutorial4, 9);
                Canvas.SetZIndex(Tutorial5, 9);
                Canvas.SetZIndex(TutorialFinish, 9);
                /*Canvas.SetZIndex(Tutorial6, 9);*/

                FoggyRect.Visibility = Visibility.Hidden;
                BackgroundRect0.Visibility = Visibility.Hidden;
                BackgroundRect1.Visibility = Visibility.Hidden;
                BackgroundRect2.Visibility = Visibility.Hidden;
                BackgroundRect3.Visibility = Visibility.Hidden;
                BackgroundRect4.Visibility = Visibility.Hidden;
                BackgroundRect5.Visibility = Visibility.Hidden;
                /*BackgroundRect6.Visibility = Visibility.Hidden;*/
                //TutorialHomeDialog.Visibility = Visibility.Hidden;
                Tutorial.Visibility = Visibility.Hidden;
                Tutorial0.Visibility = Visibility.Hidden;
                Tutorial1.Visibility = Visibility.Hidden;
                Tutorial2.Visibility = Visibility.Hidden;
                Tutorial3.Visibility = Visibility.Hidden;
                Tutorial4.Visibility = Visibility.Hidden;
                Tutorial5.Visibility = Visibility.Hidden;
                TutorialFinish.Visibility = Visibility.Hidden;
                /*Tutorial6.Visibility = Visibility.Hidden;*/
            }
        }

        private void FilterState2_Click(object sender, RoutedEventArgs e)
        {
            if (FilterState != null)
                FilterState.IsChecked = FilterState2.IsChecked;
        }

        private void FinishTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            DataLists.tutorialOn = false;
            TutorialFileLogger.WriteInFile();
            

            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 12);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Hidden;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StartTutorial_ButtonCLick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 12);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Visible;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }


        private void StepTutorial0_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 12);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 11);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Visible;
            BackgroundRect0.Opacity = 0.5;

            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Visible;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorial1_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 12);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 11);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Visible;
            BackgroundRect1.Opacity = 0.5;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Visible;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorial2_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 12);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 11);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Visible;
            BackgroundRect2.Opacity = 0.5;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Visible;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorial3_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 12);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 11);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Visible;
            BackgroundRect3.Opacity = 0.5;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Visible;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorial4_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 12);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 11);
            Canvas.SetZIndex(BackgroundRect5, 9);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            
            BackgroundRect4.Visibility = Visibility.Visible;
            BackgroundRect4.Opacity = 0.5;
            BackgroundRect5.Visibility = Visibility.Hidden;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Visible;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorial5_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 12);
            Canvas.SetZIndex(TutorialFinish, 9);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 11);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;

            BackgroundRect4.Visibility = Visibility.Hidden;
            
            BackgroundRect5.Visibility = Visibility.Visible;
            BackgroundRect5.Opacity = 0.5;
            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Visible;
            TutorialFinish.Visibility = Visibility.Hidden;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        private void StepTutorialFinish_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(Tutorial, 9);
            Canvas.SetZIndex(FoggyRect, 10);

            Canvas.SetZIndex(TableDialog, 9);
            Canvas.SetZIndex(Tutorial0, 9);
            Canvas.SetZIndex(Tutorial1, 9);
            Canvas.SetZIndex(Tutorial2, 9);
            Canvas.SetZIndex(Tutorial3, 9);
            Canvas.SetZIndex(Tutorial4, 9);
            Canvas.SetZIndex(Tutorial5, 9);
            Canvas.SetZIndex(TutorialFinish, 12);
            /*Canvas.SetZIndex(Tutorial6, 9);*/

            Canvas.SetZIndex(BackgroundRect0, 9);
            Canvas.SetZIndex(BackgroundRect1, 9);
            Canvas.SetZIndex(BackgroundRect2, 9);
            Canvas.SetZIndex(BackgroundRect3, 9);
            Canvas.SetZIndex(BackgroundRect4, 9);
            Canvas.SetZIndex(BackgroundRect5, 11);
            /*Canvas.SetZIndex(BackgroundRect6, 9);*/
            // Ostali slojevi

            FoggyRect.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;

            BackgroundRect0.Visibility = Visibility.Hidden;
            BackgroundRect1.Visibility = Visibility.Hidden;
            BackgroundRect2.Visibility = Visibility.Hidden;
            BackgroundRect3.Visibility = Visibility.Hidden;
            BackgroundRect4.Visibility = Visibility.Hidden;
            BackgroundRect5.Visibility = Visibility.Hidden;

            /*BackgroundRect6.Visibility = Visibility.Hidden;*/

            //TutorialHomeDialog.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Tutorial0.Visibility = Visibility.Hidden;
            Tutorial1.Visibility = Visibility.Hidden;
            Tutorial2.Visibility = Visibility.Hidden;
            Tutorial3.Visibility = Visibility.Hidden;
            Tutorial4.Visibility = Visibility.Hidden;
            Tutorial5.Visibility = Visibility.Hidden;
            TutorialFinish.Visibility = Visibility.Visible;
            /*Tutorial6.Visibility = Visibility.Hidden;*/
        }

        /// <summary>
        /// 
        /// </summary>
        public TableViewModel ViewModel => DataContext as TableViewModel;

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void doThings(string param)
        {
            //btnOK.Background = new SolidColorBrush(Color.FromRgb(32, 64, 128));
            //Title = param;
        }

        private async void AddNewItemPopupButton_OnClick(object sender, RoutedEventArgs e)
        {        
            var sampleMessageDialog = new SpeciesDialog
            {
                //Message = { Text = ((ButtonBase)sender).Content.ToString() }
            };
            SpeciesDialogViewModel.Tags = new ObservableCollection<Tag>();
            foreach (Tag tag in DataLists.tagList)
                SpeciesDialogViewModel.Tags.Add(tag);
            await DialogHost.Show(sampleMessageDialog, "RootDialog");

            if (sampleMessageDialog.yesSelected)
            {
                
                DataLists.speciesList.Add(sampleMessageDialog.newSpecies);

                SelectableViewModel newSvm = (new SelectableViewModel()).convertSpeciesToTableItem(sampleMessageDialog.newSpecies);
                ViewModel.TableSpecies.Add(newSvm);
                sampleMessageDialog.yesSelected = false;
                SpeciesFileLogger.WriteInFile();
            }
        }

        private async void EditItemPopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedSpecies == null)
            {
                var sampleMessageDialog = new NoSelectionDialog
                {
                    //Message = { Text = ((ButtonBase)sender).Content.ToString() }
                };

                await DialogHost.Show(sampleMessageDialog, "RootDialog");
            }
            else
            {
                var svm = ViewModel.SelectedSpecies as SelectableViewModel;
                //String oldId = svm.ID;

                Species speciesData = DataLists.FindSpeciesById(svm.ID);

                string imgPath = (string)speciesData.Image;
                if (imgPath.Equals(""))
                {
                    imgPath = speciesData.Type.Image;
                }

                

                var sampleMessageDialog = new SpeciesEditDialog
                {
                    SpeciesId = { Text = (speciesData.Id) },
                    SpeciesName = { Text = (speciesData.Name) },
                    // TODO EDIT date
                    
                    SpeciesDate = { SelectedDate = (speciesData.DiscoveryDate) },
                    //SpeciesShowDate = { Text = ((SpeciesDialogViewModel)DataContext).Date },
                    SpeciesType = { SelectedItem = (speciesData.Type), SelectedValue = (speciesData.Type.Name) },
                    SpeciesTags = { ItemsSource = speciesData.TagsOfSpecies },
                    SpeciesEndangeredStatus = { SelectedItem = (speciesData.EndangeredStatus) },
                    SpeciesTouristStatus = { SelectedItem = (speciesData.TouristStatus) },
                    SpeciesAnnualIncome = { Text = (speciesData.AnnualTourismIncome.ToString()) },
                    SpeciesDescription = { Text = (speciesData.Description) },
                    SpeciesImage = { Source = new BitmapImage(new Uri(imgPath, imgPath.Substring(0,2).Equals("C:")?UriKind.Absolute : UriKind.Relative)) },
                    
                    SpeciesIsDangerousYES = { IsChecked = (speciesData.IsDangerous == true ? true : false) },
                    SpeciesIsDangerousNO = { IsChecked = (speciesData.IsDangerous == true ? false : true) },
                    SpeciesIsOnIUCNYES = { IsChecked = (speciesData.IsOnIUCNRedList == true ? true : false) },
                    SpeciesIsOnIUCNNO = { IsChecked = (speciesData.IsOnIUCNRedList == true ? false : true) },
                    SpeciesLivesInYES = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? true : false) },
                    SpeciesLivesInNO = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? false : true) }
                };
                SpeciesDialogViewModel.Date = speciesData.DiscoveryDate;
                //SpeciesDialogViewModel.SpeciesTags = speciesData.TagsOfSpecies;
                //MessageBox.Show("Tags: "+SpeciesDialogViewModel.Tags.Count);
                SpeciesDialogViewModel.Tags = new ObservableCollection<Tag>();
                foreach (Tag tag in DataLists.tagList)
                    SpeciesDialogViewModel.Tags.Add(tag);

                SpeciesDialogViewModel.SpeciesTags = speciesData.TagsOfSpecies;
                for (int i = 0; i < SpeciesDialogViewModel.SpeciesTags.Count; i++)
                {
                    int j = 0;
                    while ( j< SpeciesDialogViewModel.Tags.Count)
                    {
                        if (SpeciesDialogViewModel.SpeciesTags[i].Id.Equals(SpeciesDialogViewModel.Tags[j].Id))
                        {
                            SpeciesDialogViewModel.Tags.Remove(SpeciesDialogViewModel.Tags[j]);
                        }
                        else
                        {
                            j++;
                        }
                        
                    }
                    
                }
                
                await DialogHost.Show(sampleMessageDialog, "RootDialog");

                if (sampleMessageDialog.yesSelected)
                {
                    int indexDL = DataLists.speciesList.IndexOf(DataLists.FindSpeciesById(svm.ID));
                    DataLists.speciesList[indexDL] = sampleMessageDialog.editedSpecies;
                    
                    SelectableViewModel newSvm = (new SelectableViewModel()).convertSpeciesToTableItem(sampleMessageDialog.editedSpecies);
                    int indexVM = ViewModel.TableSpecies.IndexOf(svm);
                    ViewModel.TableSpecies[indexVM] = newSvm;
                    
                    sampleMessageDialog.yesSelected = false;
                    SpeciesFileLogger.WriteInFile();


                }
            }
        }


        private async void ItemDetailsPopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedSpecies == null)
            {
                var sampleMessageDialog = new NoSelectionDialog
                {
                    //Message = { Text = ((ButtonBase)sender).Content.ToString() }
                };

                await DialogHost.Show(sampleMessageDialog, "RootDialog");
            }
            else
            {
                var svm = ViewModel.SelectedSpecies as SelectableViewModel;
                

                Species speciesData = DataLists.FindSpeciesById(svm.ID);

                string imgPath = (string)speciesData.Image;
                if (imgPath.Equals(""))
                {
                    imgPath = speciesData.Type.Image;
                }

               

                var sampleMessageDialog = new SpeciesDetailsDialog
                {
                    SpeciesId = { Text = (speciesData.Id) },
                    SpeciesName = { Text = (speciesData.Name) },
                    SpeciesDate = { SelectedDate = (speciesData.DiscoveryDate) },
                    SpeciesType = { SelectedItem = (speciesData.Type), SelectedValue = (speciesData.Type.Name) },
                    SpeciesEndangeredStatus = { SelectedItem = (speciesData.EndangeredStatus) },
                    SpeciesTouristStatus = { SelectedItem = (speciesData.TouristStatus) },
                    SpeciesAnnualIncome = { Text = (speciesData.AnnualTourismIncome.ToString()) },
                    SpeciesDescription = { Text = (speciesData.Description) },
                    SpeciesImage = { Source = new BitmapImage(new Uri(imgPath, imgPath.Substring(0, 2).Equals("C:") ? UriKind.Absolute : UriKind.Relative)) },
                    //SpeciesTags = { Text = (speciesData.Tags) },
                    SpeciesIsDangerousYES = { IsChecked = (speciesData.IsDangerous == true? true : false ) },
                    SpeciesIsDangerousNO = { IsChecked = (speciesData.IsDangerous == true ? false : true) },
                    SpeciesIsOnIUCNYES = { IsChecked = (speciesData.IsOnIUCNRedList == true ? true : false) },
                    SpeciesIsOnIUCNNO = { IsChecked = (speciesData.IsOnIUCNRedList == true ? false : true) },
                    SpeciesLivesInYES = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? true : false) },
                    SpeciesLivesInNO = { IsChecked = (speciesData.LivesInPopulatedRegion == true ? false : true) },

                };

                SpeciesDialogViewModel.SpeciesTags = speciesData.TagsOfSpecies;

                await DialogHost.Show(sampleMessageDialog, "RootDialog");
            }
        }

        private async void DeleteItemPopupButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedSpecies == null)
            {
                var sampleMessageDialog = new NoSelectionDialog
                {
                    //Message = { Text = ((ButtonBase)sender).Content.ToString() }
                };

                await DialogHost.Show(sampleMessageDialog, "RootDialog");
            }
            else
            {
                //var svm = ViewModel.SelectedSpecies as SelectableViewModel;

                //Species species = new Species();
                //Species speciesData = species.findSpeciesById(svm.ID);

                var sampleMessageDialog = new SpeciesDeleteDialog
                (
                    //Message = { Text = ((ButtonBase)sender).Content.ToString() }
                    //speciesData,
                    //ViewModel.SelectedSpecies as SelectableViewModel
                );
                
                await DialogHost.Show(sampleMessageDialog, "RootDialog");

                if (sampleMessageDialog.yesSelected)
                {
                    /*var svm = ViewModel.SelectedSpecies as SelectableViewModel;

                    Species speciesToRemove = DataLists.findSpeciesById(svm.ID);
                    ObservableCollection<Species> speciesList = DataLists.speciesList;
                    speciesList.Remove(speciesToRemove);
                    ViewModel.TableSpecies.Remove(ViewModel.SelectedSpecies as SelectableViewModel);
                    sampleMessageDialog.yesSelected = false;
                    SpeciesFileLogger.WriteInFile();*/

                    for (int i = 0; i < selectedSpecies.Count; i++)
                    {
                        ViewModel.TableSpecies.Add(selectedSpecies[i]);

                    }

                    for (int i = 0; i < selectedSpecies.Count; i++)
                    {
                        if (selectedSpecies.Count != 0)
                        {
                            int j = 0;
                            while (j < ViewModel.TableSpecies.Count)
                            {
                                if (selectedSpecies.Count != 0)
                                {
                                    if (selectedSpecies[i].ID.Equals(ViewModel.TableSpecies[j].ID))
                                    {
                                        ViewModel.TableSpecies.Remove(ViewModel.TableSpecies[j]);
                                    }
                                    else
                                    {
                                        j++;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    }


                    selectedSpecies.Clear();
                    sampleMessageDialog.yesSelected = false;
                    SpeciesFileLogger.WriteInFile();
                }
                
            }
        }

        private async void FilterSettingsButton_OnClick(object sender, RoutedEventArgs e)
        {
            DateTime NullDate = DateTime.MinValue;
            
            var sampleMessageDialog = new FilterSettingsDialog
            {

                SpeciesType = { SelectedItem = newFiltededSearch == null ? FilterSettingsDialogViewModel.SpeciesTypesList.Last() : (newFiltededSearch.Type.Id.Equals("") ? FilterSettingsDialogViewModel.SpeciesTypesList.Last() : newFiltededSearch.Type),
                    SelectedValue = newFiltededSearch == null ? FilterSettingsDialogViewModel.SpeciesTypesList.Last().Name : (newFiltededSearch.Type.Id.Equals("") ? FilterSettingsDialogViewModel.SpeciesTypesList.Last().Name : newFiltededSearch.Type.Name) },
                SpeciesEndangeredStatus = { SelectedItem = newFiltededSearch == null ? "All":(newFiltededSearch.EndangeredStatus) },
                SpeciesTouristStatus = { SelectedItem = newFiltededSearch == null ? "All" : (newFiltededSearch.TouristStatus) },

                SpeciesDateFROM = { SelectedDate = newFiltededSearch == null ? NullDate : (newFiltededSearch.DiscoveryDateFROM) },
                SpeciesDateTO = { SelectedDate = newFiltededSearch == null ? NullDate : (newFiltededSearch.DiscoveryDateTO) },

                SpeciesAnnualIncomeFROM = { Text = newFiltededSearch == null ? 0.ToString() : (newFiltededSearch.AnnualTourismIncomeFROM.ToString()) },
                SpeciesAnnualIncomeTO = { Text = newFiltededSearch == null ? 0.ToString() : (newFiltededSearch.AnnualTourismIncomeTO.ToString()) },

                SpeciesIsDangerousYES = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.IsDangerousChoice.Equals("Yes") ? true : false) },
                SpeciesIsDangerousNO = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.IsDangerousChoice.Equals("No") ? true : false) },
                SpeciesIsDangerousOther = { IsChecked = newFiltededSearch == null ? true : (newFiltededSearch.IsDangerousChoice.Equals("Other") ? true : false) },

                SpeciesIsOnIUCNYES = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.IsOnIUCNRedListChoice.Equals("Yes") ? true : false) },
                SpeciesIsOnIUCNNO = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.IsOnIUCNRedListChoice.Equals("No") ? true : false) },
                SpeciesIsOnIUCNOther = { IsChecked = newFiltededSearch == null ? true : (newFiltededSearch.IsOnIUCNRedListChoice.Equals("Other") ? true : false) },

                SpeciesLivesInYES = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.LivesInPopulatedRegionChoice.Equals("Yes") ? true : false) },
                SpeciesLivesInNO = { IsChecked = newFiltededSearch == null ? false : (newFiltededSearch.LivesInPopulatedRegionChoice.Equals("No") ? true : false) },
                SpeciesLivesInOther = { IsChecked = newFiltededSearch == null ? true : (newFiltededSearch.LivesInPopulatedRegionChoice.Equals("Other") ? true : false) },

            };
            
            await DialogHost.Show(sampleMessageDialog, "RootDialog");

            if (sampleMessageDialog.saveSelected)
            {

                //DataLists.speciesList.Add(sampleMessageDialog.newSpecies);

                //SelectableViewModel newSvm = (new SelectableViewModel()).convertSpeciesToTableItem(sampleMessageDialog.newSpecies);
                //ViewModel.TableSpecies.Add(newSvm);
                sampleMessageDialog.saveSelected = false;
                newFiltededSearch = new FilteredSearch(sampleMessageDialog.filteredSearch.Type, sampleMessageDialog.filteredSearch.EndangeredStatus, 
                    sampleMessageDialog.filteredSearch.IsDangerousChoice, sampleMessageDialog.filteredSearch.IsOnIUCNRedListChoice, 
                    sampleMessageDialog.filteredSearch.LivesInPopulatedRegionChoice, sampleMessageDialog.filteredSearch.TouristStatus, sampleMessageDialog.filteredSearch.DiscoveryDateFROM,
                    sampleMessageDialog.filteredSearch.DiscoveryDateTO, sampleMessageDialog.filteredSearch.AnnualTourismIncomeFROM, sampleMessageDialog.filteredSearch.AnnualTourismIncomeTO);

                FilterState.IsChecked = true;
                //SpeciesFileLogger.WriteInFile();
            }
        }

        private void TextToSearch2_TextChanged(object sender, RoutedEventArgs e)
        {
            
            if (TextToSearch != null)
            {
                TextToSearch.Text = TextToSearch2.Text.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SearchButton_OnClick(object sender, RoutedEventArgs e)
        {
            ProgressBar.Visibility = Visibility.Visible;

            ViewModel.RefreshTable();
            DateTime NullDate = DateTime.MinValue;

            int i = 0;
            while (i < ViewModel.TableSpecies.Count)
            {
                
                bool remove1 = false;
                bool remove2 = false;
                bool remove3 = false;
                bool remove4 = false;
                bool remove5 = false;
                bool remove6 = false;
                bool remove7 = false;
                bool remove8 = false;
                bool remove9 = false;
                bool remove10 = false;
                bool remove11 = false;

                if (ViewModel.TableSpecies[i].ID.IndexOf(TextToSearch.Text.ToString(), StringComparison.OrdinalIgnoreCase) >= 0 ||
                    ViewModel.TableSpecies[i].Name.IndexOf(TextToSearch.Text.ToString(), StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    //i++;
                }
                else
                {
                    //ViewModel.TableSpecies.RemoveAt(i);
                    remove1 = true;
                }


                if (FilterState.IsChecked == true && newFiltededSearch!=null)
                {
                    if (newFiltededSearch.Type != null && !newFiltededSearch.Type.Name.Equals(""))
                    { 
                        if (newFiltededSearch.Type.Name.Equals("All"))
                        {

                        }
                        else
                        {
                            if (newFiltededSearch.Type.Name.Equals(ViewModel.TableSpecies[i].Type))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove2 = true;
                            }
                        }
                    }
                    if (newFiltededSearch.EndangeredStatus != null && !newFiltededSearch.EndangeredStatus.Equals(""))
                    {
                        if (newFiltededSearch.EndangeredStatus.Equals("All"))
                        {

                        }
                        else
                        {
                            if (newFiltededSearch.EndangeredStatus.Equals(ViewModel.TableSpecies[i].EndangeredStatus))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove3 = true;
                            }
                        }
                    }

                    if (newFiltededSearch.IsDangerousChoice != null && !newFiltededSearch.IsDangerousChoice.Equals(""))
                    {
                        if (newFiltededSearch.IsDangerousChoice.Equals("Other"))
                        {

                        } else
                        {
                            if (newFiltededSearch.IsDangerousChoice.Equals(ViewModel.TableSpecies[i].Dangerous))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove4 = true;
                            }
                        }
                        
                    }

                    if (newFiltededSearch.IsOnIUCNRedListChoice != null && !newFiltededSearch.IsOnIUCNRedListChoice.Equals(""))
                    {
                        if (newFiltededSearch.IsOnIUCNRedListChoice.Equals("Other"))
                        {

                        }
                        else
                        {
                            if (newFiltededSearch.IsOnIUCNRedListChoice.Equals(ViewModel.TableSpecies[i].OnIUCNRedList))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove5 = true;
                            }
                        }

                    }

                    if (newFiltededSearch.LivesInPopulatedRegionChoice != null && !newFiltededSearch.LivesInPopulatedRegionChoice.Equals(""))
                    {
                        if (newFiltededSearch.LivesInPopulatedRegionChoice.Equals("Other"))
                        {

                        }
                        else
                        {
                            if (newFiltededSearch.LivesInPopulatedRegionChoice.Equals(DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).LivesInPopulatedRegion))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove6 = true;
                            }
                        }

                    }

                    if (newFiltededSearch.TouristStatus != null && !newFiltededSearch.TouristStatus.Equals(""))
                    {
                        if (newFiltededSearch.TouristStatus.Equals("All"))
                        {

                        }
                        else
                        {
                            if (newFiltededSearch.TouristStatus.Equals(DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).TouristStatus))
                            {

                            }
                            else
                            {
                                //ViewModel.TableSpecies.RemoveAt(i);
                                remove7 = true;
                            }
                        }

                    }

                    //TODO for date
                    
                    if (DateTime.Compare(newFiltededSearch.DiscoveryDateFROM,NullDate)!=0)
                    {
                        if (DateTime.Compare(newFiltededSearch.DiscoveryDateFROM, DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).DiscoveryDate) > 0)
                        {
                            remove8 = true;
                        } 
                    }
                    if (DateTime.Compare(newFiltededSearch.DiscoveryDateTO, NullDate) != 0)
                    {
                        if (DateTime.Compare(newFiltededSearch.DiscoveryDateTO, DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).DiscoveryDate) < 0)
                        {
                            remove9 = true;
                        }
                    }


                    if (!newFiltededSearch.AnnualTourismIncomeFROM.Equals(""))
                    {
                        if (newFiltededSearch.AnnualTourismIncomeFROM != 0)
                        {
                            if (DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).AnnualTourismIncome < newFiltededSearch.AnnualTourismIncomeFROM)
                            {
                                remove10 = true;
                            }
                        }    
                        else
                        {

                        }
                       
                    }
                    if (!newFiltededSearch.AnnualTourismIncomeTO.Equals(""))
                    {
                        if (newFiltededSearch.AnnualTourismIncomeTO != 0)
                        {
                            if (DataLists.FindSpeciesById(ViewModel.TableSpecies[i].ID).AnnualTourismIncome > newFiltededSearch.AnnualTourismIncomeTO)
                            {
                                remove11 = true;
                            }
                        }
                        else
                        {

                        }
                    }

                    if (remove1 || remove2 || remove3 || remove4 || remove5 || remove6 || remove7 || remove8 || remove9 || remove10 || remove11)
                    {
                        ViewModel.TableSpecies.RemoveAt(i);

                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    if (remove1)
                    {
                        ViewModel.TableSpecies.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                


            }
            
            ProgressBar.Visibility = Visibility.Hidden;
        }


        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void DisposeSearchButton_OnClick(object sender, RoutedEventArgs e)
        {
            TextToSearch.Text = "";
            newFiltededSearch = null;
            ViewModel.RefreshTable();
        }

        /// <summary>
        /// Asks for confirmation of a delete.
        /// </summary>
        /// <param name="sender">Sender of the event: the DataGrid.</param>
        /// <param name="e">Event arguments.</param>
        private void DriversDataGrid_PreviewDeleteCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == DataGrid.DeleteCommand)
            {
                if (!(MessageBox.Show("Are you sure you want to delete?", "Please confirm.", MessageBoxButton.YesNo) == MessageBoxResult.Yes))
                {
                    // Cancel Delete.
                    e.Handled = true;
                }
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (SelectableViewModel item in e.RemovedItems)
            {
                selectedSpecies.Remove(item);
            }

            foreach (SelectableViewModel item in e.AddedItems)
            {
                selectedSpecies.Add(item);
                if (DataLists.tutorialOn == true)
                {
                    From0To1.IsEnabled = true;
                }
            }
        }

        
    }
}
