﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using EndangeredSpecies.Domain;
using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;
using System.Runtime.CompilerServices;


namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     For Filtered Search dialog
    /// </summary>
    public class FilterSettingsDialogViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// 
        /// </summary>
        public FilterSettingsDialogViewModel()
        {
            SpeciesTypesList = new ObservableCollection<SpeciesType>();
            for (int i = 0; i< DataLists.speciesTypeList.Count; i++)
            {
                SpeciesTypesList.Add(DataLists.speciesTypeList[i]);
            }
            SpeciesType allSpecies = new SpeciesType();
            allSpecies.Name = "All";
            SpeciesTypesList.Add(allSpecies);

            EndangeredStatuses = FilteredSearch.EndageredStatuses;

            TouristStatuses = FilteredSearch.TouristStatuses;

            DateFROM = new DateTime();
            DateTO = new DateTime();
            
        }

        /// <summary>
        /// 
        /// </summary>
        public static ObservableCollection<SpeciesType> SpeciesTypesList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static ObservableCollection<string> EndangeredStatuses { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static ObservableCollection<string> TouristStatuses { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static DateTime DateFROM { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static DateTime DateTO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
