﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     Checking the ID empty field or not
    /// </summary>
    public static class NotifyPropertyChangedExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TField"></typeparam>
        /// <param name="instance"></param>
        /// <param name="field"></param>
        /// <param name="newValue"></param>
        /// <param name="raise"></param>
        /// <param name="propertyName"></param>
        public static void MutateVerbose<TField>(this INotifyPropertyChanged instance, ref TField field, TField newValue, Action<PropertyChangedEventArgs> raise, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<TField>.Default.Equals(field, newValue)) return;
            field = newValue;
            raise?.Invoke(new PropertyChangedEventArgs(propertyName));
        }
    }
}
