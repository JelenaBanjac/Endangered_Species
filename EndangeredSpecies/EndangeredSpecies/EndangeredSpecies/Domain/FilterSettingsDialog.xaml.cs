﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Domain;
using EndangeredSpecies.Logger;
using MaterialDesignThemes.Wpf;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     Interaction logic for FilterSettingsDialog.xaml
    /// </summary>
    public partial class FilterSettingsDialog : UserControl
    {
        /// <summary>
        ///    Filtered search parameters are saved 
        /// </summary>
        public bool saveSelected = false;

        /// <summary>
        ///     Object that is created when user wants to use filtered search
        /// </summary>
        public FilteredSearch filteredSearch = null;

        /// <summary>
        ///     Default constructor
        /// </summary>
        public FilterSettingsDialog()
        {
            DataLists dl = new DataLists();
            InitializeComponent();
            DataContext = new FilterSettingsDialogViewModel();
        }

        /// <summary>
        ///     What to do with window on opening for choosing FROM date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void FROMCalendarDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {

            SpeciesDateFROM.SelectedDate = FilterSettingsDialogViewModel.DateFROM;
        }

        /// <summary>
        ///     What to do with window o closing for choosing FROM date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void FROMCalendarDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (!Equals(eventArgs.Parameter, "1")) return;

            if (!SpeciesDateFROM.SelectedDate.HasValue)
            {
                eventArgs.Cancel();
                return;
            }

            FilterSettingsDialogViewModel.DateFROM = SpeciesDateFROM.SelectedDate.Value;
            SpeciesShowDateFROM.Text = FilterSettingsDialogViewModel.DateFROM.ToString("d");
        }

        /// <summary>
        ///     What to do with window on opening for choosing TO date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void TOCalendarDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {
            SpeciesDateTO.SelectedDate = FilterSettingsDialogViewModel.DateFROM;
        }

        /// <summary>
        ///     What to do with window o closing for choosing TO date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        public void TOCalendarDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (!Equals(eventArgs.Parameter, "1")) return;

            if (!SpeciesDateTO.SelectedDate.HasValue)
            {
                eventArgs.Cancel();
                return;
            }

            FilterSettingsDialogViewModel.DateTO = SpeciesDateTO.SelectedDate.Value;
            SpeciesShowDateTO.Text = FilterSettingsDialogViewModel.DateTO.ToString("d");
        }

        private void Check_Field(object sender, RoutedEventArgs e)
        {
            
            double annualIncome = 0;
            if (!SpeciesAnnualIncomeFROM.Text.ToString().Equals(""))
            {
                if (double.TryParse(SpeciesAnnualIncomeFROM.Text.ToString(), out annualIncome))
                {
                    if (!SpeciesAnnualIncomeTO.Text.ToString().Equals(""))
                    {
                        if (double.TryParse(SpeciesAnnualIncomeTO.Text.ToString(), out annualIncome))
                        {
                            SaveButton.IsEnabled = true;
                        }
                        else
                        {
                            SaveButton.IsEnabled = false;
                        }
                    }
                    else
                    {
                        SaveButton.IsEnabled = true;
                    }
                }
                else
                {
                    SaveButton.IsEnabled = false;
                }
            }
            else
            {
                if (!SpeciesAnnualIncomeTO.Text.ToString().Equals(""))
                {
                    if (double.TryParse(SpeciesAnnualIncomeTO.Text.ToString(), out annualIncome))
                    {
                        SaveButton.IsEnabled = true;
                    }
                    else
                    {
                        SaveButton.IsEnabled = false;
                    }
                }
                else
                {
                    SaveButton.IsEnabled = true;
                }
 
            }
            
        }

        private void SaveFilterSettings_OnClick(object sender, RoutedEventArgs e)
        {
            saveSelected = true;

            filteredSearch = new FilteredSearch();
            if (SpeciesDateFROM.SelectedDate.HasValue)
            {
                filteredSearch.DiscoveryDateFROM = (DateTime)SpeciesDateFROM.SelectedDate;
            } else
            {
                DateTime NullDate = DateTime.MinValue;
                //DateTime dateOfBirth = NullDate;
                filteredSearch.DiscoveryDateFROM = NullDate;
            }

            if (SpeciesDateTO.SelectedDate.HasValue)
            {
                filteredSearch.DiscoveryDateTO = (DateTime)SpeciesDateTO.SelectedDate;
            }
            else
            {
                DateTime NullDate = DateTime.MinValue;
                //DateTime dateOfBirth = NullDate;
                filteredSearch.DiscoveryDateTO = NullDate;
            }
            //filteredSearch.DiscoveryDateTO = (DateTime)SpeciesDateTO.SelectedDate;
            if (((SpeciesType)SpeciesType.SelectedItem) == null)
                filteredSearch.Type = null;
            else
                filteredSearch.Type = DataLists.FindTypeByName(((SpeciesType)SpeciesType.SelectedItem).Name.ToString());
            
            filteredSearch.EndangeredStatus = SpeciesEndangeredStatus.Text.ToString();
            filteredSearch.TouristStatus = SpeciesTouristStatus.Text.ToString();

            double annualIncomeFROM = 0;
            if (!double.TryParse(SpeciesAnnualIncomeFROM.Text.ToString(), out annualIncomeFROM))
                filteredSearch.AnnualTourismIncomeFROM = 0;
            filteredSearch.AnnualTourismIncomeFROM = annualIncomeFROM;
            double annualIncomeTO = 0;
            if (!double.TryParse(SpeciesAnnualIncomeTO.Text.ToString(), out annualIncomeTO))
                filteredSearch.AnnualTourismIncomeTO = 0;
            filteredSearch.AnnualTourismIncomeTO = annualIncomeTO;

            if ((bool)SpeciesIsDangerousOther.IsChecked==true)
            {
                filteredSearch.IsDangerousChoice = "Other";
            }
            else
            {
                filteredSearch.IsDangerousChoice = (bool)SpeciesIsDangerousYES.IsChecked?"Yes":"No";
            }

            if ((bool)SpeciesIsOnIUCNOther.IsChecked == true)
            {
                filteredSearch.IsOnIUCNRedListChoice = "Other";
            }
            else
            {
                filteredSearch.IsOnIUCNRedListChoice = (bool)SpeciesIsOnIUCNYES.IsChecked ? "Yes" : "No";
            }

            if ((bool)SpeciesLivesInOther.IsChecked == true)
            {
                filteredSearch.LivesInPopulatedRegionChoice = "Other";
            }
            else
            {
                filteredSearch.LivesInPopulatedRegionChoice = (bool)SpeciesLivesInYES.IsChecked ? "Yes" : "No";
            }
            
        }
    }

    
}
