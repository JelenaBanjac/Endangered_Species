﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using EndangeredSpecies.Domain;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     ViewModel for species type
    /// </summary>
    public class SpeciesTypeDialogViewModel : INotifyPropertyChanged
    {
        
        private string _name;
        private int _selectedValueOne;

        /// <summary>
        ///     Default constructor
        /// </summary>
        public SpeciesTypeDialogViewModel()
        {
            
        }

        /// <summary>
        ///     Gets or sets name of species type
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                this.MutateVerbose(ref _name, value, RaisePropertyChanged());
            }
        }

        /// <summary>
        ///     Gets or sets seleced value
        /// </summary>
        public int SelectedValueOne
        {
            get { return _selectedValueOne; }
            set
            {
                this.MutateVerbose(ref _selectedValueOne, value, RaisePropertyChanged());
            }
        }
        

        /// <summary>
        ///     Event handler for changing property
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
    }
}
