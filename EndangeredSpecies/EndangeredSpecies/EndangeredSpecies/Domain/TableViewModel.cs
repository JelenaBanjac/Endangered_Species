﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;
using System.Windows;


namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     View model for table of species
    /// </summary>
    public class TableViewModel : INotifyPropertyChanged
    {
 
        private ObservableCollection<SelectableViewModel> tableSpecies;

        private object _selectedItem;
        
        /// <summary>
        ///     List of all species
        /// </summary>
        public ObservableCollection<Species> AllSpecies { get; }

        /// <summary>
        /// 
        /// </summary>
        public CommandImplementation AddCommand { get; }

        /// <summary>
        /// 
        /// </summary>
        public CommandImplementation RemoveSelectedItemCommand { get; } 

        /// <summary>
        ///     Default constuctor
        /// </summary>
        public TableViewModel()
        {
            tableSpecies = CreateData();       
        }
        

        /// <summary>
        /// 
        /// </summary>
        public object SelectedSpecies
        {
            get { return _selectedItem; }
            set { _selectedItem = value; NotifyPropertyChanged("SelectedSpecies"); }
        }

        /// <summary>
        ///     Event handler for property changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        ///     Notifies the property changed.
        /// </summary>
        /// <param name="info">The info.</param>
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<SelectableViewModel> CreateData()
        {
            ObservableCollection<SelectableViewModel> tableItems = new ObservableCollection<SelectableViewModel>();
            SpeciesFileLogger.ReadFile();
            ObservableCollection<Species> speciesData = DataLists.speciesList;

            foreach (var data in speciesData)
            {
                SelectableViewModel svm = new SelectableViewModel();
                svm.ID = data.Id;
                svm.Name = data.Name;
                svm.Type = data.Type.Name;
                svm.EndangeredStatus = data.EndangeredStatus;
                svm.Dangerous = data.IsDangerous==true ? "Yes":"No";
                svm.OnIUCNRedList = data.IsOnIUCNRedList == true ? "Yes" : "No";

                tableItems.Add(svm);
            }

            return tableItems;
            
        }

        /// <summary>
        /// 
        /// </summary>
        public void RefreshTable()
        {
            //ObservableCollection<SelectableViewModel> tableItems = new ObservableCollection<SelectableViewModel>();
            tableSpecies.Clear();
            SpeciesFileLogger.ReadFile();
            ObservableCollection<Species> speciesData = DataLists.speciesList;

            foreach (var data in speciesData)
            {
                SelectableViewModel svm = new SelectableViewModel();
                svm.ID = data.Id;
                svm.Name = data.Name;
                svm.Type = data.Type.Name;
                svm.EndangeredStatus = data.EndangeredStatus;
                svm.Dangerous = data.IsDangerous == true ? "Yes" : "No";
                svm.OnIUCNRedList = data.IsOnIUCNRedList == true ? "Yes" : "No";

                tableSpecies.Add(svm);
            }

        }


        
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<SelectableViewModel> TableSpecies
        {
            get { return tableSpecies; }
            set { tableSpecies = value; }
        }

        
    }
}
