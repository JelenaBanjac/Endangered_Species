﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using EndangeredSpecies.Classes;

namespace EndangeredTag.Domain
{
    /// <summary>
    ///     For appearance of tag in tag list
    /// </summary>
    public class SelectableTagViewModel : INotifyPropertyChanged
    {
        private string _id;
        private string _description;
        private string _color;
        
        /// <summary>
        ///     Method that converts tag to selectable tag (adaptation).
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public SelectableTagViewModel convertTagToTableItem(Tag tag)
        {
            SelectableTagViewModel svm = new SelectableTagViewModel();
            svm.ID = tag.Id.ToString();
            svm.Description = tag.Description.ToString();
            svm.Color = tag.Color.ToString();
            
            return svm;
        }


        /// <summary>
        ///     Gets or sets tag ID inside the list
        /// </summary>
        public string ID
        {
            get { return _id; }
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        
        /// <summary>
        ///     Gets or sets tag Desciption inside the list
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                if (_description == value) return;
                _description = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets tag Color inside the list
        /// </summary>
        public string Color
        {
            get { return _color; }
            set
            {
                if (_color == value) return;
                _color = value;
                OnPropertyChanged();
            }
        }

        
        /// <summary>
        ///     Event handler for property changed (clicked on the item of the list)
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Method what to do when clicked on the item of the list
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
