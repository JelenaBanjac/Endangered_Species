﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Domain;
using EndangeredSpecies.Logger;
using MaterialDesignThemes.Wpf;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    /// Interaction logic for SpeciesTypeDialog.xaml
    /// </summary>
    public partial class SpeciesTypeDialog : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public SpeciesType newSpeciesType = null;

        /// <summary>
        /// 
        /// </summary>
        public SpeciesTypeDialog()
        {
            DataLists dl = new DataLists();
            InitializeComponent();
            DataContext = new SpeciesTypeDialogViewModel();
            SaveButton.IsEnabled = false;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public string ImgPath = "";
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";
            ofd.Multiselect = false;

            Nullable<bool> ok = ofd.ShowDialog();
            if (ok == true)
            {
                ImgPath = ofd.FileName;
                BitmapImage image = new BitmapImage(new Uri(ImgPath, UriKind.Absolute));
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.Freeze();
                SpeciesTypeImage.Source = image;

            }
        }

        private void Field_Changed(object sender, RoutedEventArgs e)
        {
            if (!SpeciesTypeId.Text.ToString().Equals(""))
            {
                bool existingId = false;
                for (int i = 0; i < DataLists.speciesTypeList.Count; i++)
                {
                    if (DataLists.speciesTypeList[i].Id.Equals(SpeciesTypeId.Text.ToString()))
                    {
                        existingId = true;
                        break;
                    }
                }
                if (existingId == true)
                {
                    SaveButton.IsEnabled = false;
                }
                else
                {
                    SaveButton.IsEnabled = true;
                }
            }
            else
            {
                SaveButton.IsEnabled = false;
            }
        }

        private async void SelectedYESCommand(object sender, RoutedEventArgs e)
        {

            newSpeciesType = new SpeciesType();
            newSpeciesType.Id = SpeciesTypeId.Text.ToString();
            newSpeciesType.Name = SpeciesTypeName.Text.ToString();
            newSpeciesType.Description = SpeciesTypeDescription.Text.ToString();
            if (ImgPath.Equals(""))
            {
                ImgPath = "../Images/default.png";
            }
            newSpeciesType.Image = ImgPath;


            DataLists.speciesTypeList.Add(newSpeciesType);
            
            SpeciesTypeFileLogger.WriteInFile();

        }
    }
}
