﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;


namespace EndangeredSpecies.Domain
{
    /// <summary>
    /// Interaction logic for TagDialog.xaml
    /// </summary>
    public partial class TagDialog : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public Tag newTag = null;

        /// <summary>
        /// 
        /// </summary>
        public TagDialog()
        {
            InitializeComponent();

            DataLists dl = new DataLists();

            SaveButton.IsEnabled = false;
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            int a, r, g, b;
            if (Int32.TryParse(ClrPcker_Background.SelectedColor.Value.A.ToString(), out a) &&
                Int32.TryParse(ClrPcker_Background.SelectedColor.Value.R.ToString(), out r) && 
                Int32.TryParse(ClrPcker_Background.SelectedColor.Value.G.ToString(), out g) &&
                Int32.TryParse(ClrPcker_Background.SelectedColor.Value.B.ToString(), out b))
            {
                TagColor.Text = "#" + a.ToString("X2") + r.ToString("X2") + g.ToString("X2") + b.ToString("X2");

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SelectedYESCommand(object sender, RoutedEventArgs e)
        {

            newTag = new Tag();
            newTag.Id = TagId.Text.ToString();
            newTag.Color = TagColor.Text.ToString();
            newTag.Description = TagDescription.Text.ToString();


            DataLists.tagList.Add(newTag);

            TagFileLogger.WriteInFile();

        }

        private void IDTextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            if (!TagId.Text.ToString().Equals(""))
            {
                bool existingId = false;
                for (int i = 0; i < DataLists.tagList.Count; i++)
                {
                    if (DataLists.tagList[i].Id.Equals(TagId.Text.ToString()))
                    {
                        existingId = true;
                        break;
                    }
                }
                if (existingId == true)
                {
                    SaveButton.IsEnabled = false;
                } else
                {
                    SaveButton.IsEnabled = true;
                }
            }
            else
            {
                SaveButton.IsEnabled = false;
            }
        }

    }
}
