﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using EndangeredSpecies.Domain;
using System.Collections.ObjectModel;
using EndangeredSpecies.Classes;
using EndangeredSpecies.Logger;
using System.Runtime.CompilerServices;

namespace EndangeredSpecies.Domain
{
    /// <summary>
    ///     View model for species
    /// </summary>
    public class SpeciesDialogViewModel : INotifyPropertyChanged
    {
        
        /// <summary>
        ///     Default constuctor
        /// </summary>
        public SpeciesDialogViewModel()
        {
            SpeciesTypesList = DataLists.speciesTypeList;

            EndangeredStatuses = Species.EndageredStatuses;

            TouristStatuses = Species.TouristStatuses;

            Tags = new ObservableCollection<Tag>();

            SpeciesTags = new ObservableCollection<Tag>();

            Date = new DateTime();
            
        }

        /// <summary>
        ///     Gets or sets list of species types for species
        /// </summary>
        public static ObservableCollection<SpeciesType> SpeciesTypesList { get; set; }

        /// <summary>
        ///     Gets or sets list of endangered statuses
        /// </summary>
        public static ObservableCollection<string> EndangeredStatuses { get; set; }

        /// <summary>
        ///     Gets or sets the list of tourist statuses
        /// </summary>
        public static ObservableCollection<string> TouristStatuses { get; set; }

        /// <summary>
        ///     Gets or sets tags (all tags in system)
        /// </summary>
        public static ObservableCollection<Tag> Tags { get; set; }

        /// <summary>
        ///     Gets or sets tags (only considered species)
        /// </summary>
        public static ObservableCollection<Tag> SpeciesTags { get; set; }

        /// <summary>
        ///     Gets or sets the date
        /// </summary>
        public static DateTime Date { get; set; }

        /// <summary>
        ///     Refreshing list of tags (used for data recovery after finished search)
        /// </summary>
        public static void RefreshTags()
        {
            Tags.Clear();
            foreach (Tag tag in DataLists.tagList)
                Tags.Add(tag);

            for (int i = 0; i < SpeciesTags.Count; i++)
            {
                int j = 0;
                while (j < Tags.Count)
                {
                    if (SpeciesTags[i].Id.Equals(Tags[j].Id))
                    {
                        Tags.Remove(Tags[j]);
                    }
                    else
                    {
                        j++;
                    }

                }

            }

        }
        

        /// <summary>
        ///     Handler for changed property
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
