﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using EndangeredSpecies.Logger;

namespace EndangeredSpecies
{
    /// <summary>
    ///     Interaction logic for <c>Home.xaml</c>
    ///     Window welcomes user to the application.
    /// </summary>
    public partial class Home : UserControl
    {
        /// <summary>
        ///     <c>Home</c> window welcomes user to the application.
        /// </summary>
        public Home()
        {
            InitializeComponent();
            DataLists dl = new DataLists();
            prepareForTutorial();
        }

        /// <summary>
        ///     Prepare Home window in case user is in tutorial mode.
        /// </summary>
        public void prepareForTutorial()
        {
            if (DataLists.tutorialOn == true)
            {
                Canvas.SetZIndex(HomeDialog, 9);
                Canvas.SetZIndex(FoggyRect, 10);
                Canvas.SetZIndex(TutorialHomeDialog, 9);
                Canvas.SetZIndex(StartTutorialHomeDialog, 12);
                FoggyRect.Visibility = Visibility.Visible;
                TutorialHomeDialog.Visibility = Visibility.Hidden;
                StartTutorialHomeDialog.Visibility = Visibility.Visible;
                FoggyRect.Opacity = 0.5;
                /*Canvas.SetZIndex(HomeDialog, 9);
                Canvas.SetZIndex(FoggyRect, 10);
                Canvas.SetZIndex(TutorialHomeDialog, 12);
                FoggyRect.Visibility = Visibility.Visible;
                TutorialHomeDialog.Visibility = Visibility.Visible;
                FoggyRect.Opacity = 0.5;*/

            }
            else
            {
                Canvas.SetZIndex(HomeDialog, 14);
                Canvas.SetZIndex(FoggyRect, 10);
                Canvas.SetZIndex(TutorialHomeDialog, 12);
                Canvas.SetZIndex(StartTutorialHomeDialog, 13);
                FoggyRect.Visibility = Visibility.Hidden;
                TutorialHomeDialog.Visibility = Visibility.Hidden;
                StartTutorialHomeDialog.Visibility = Visibility.Hidden;

            }
        }

        private void StartTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            DataLists.tutorialOn = true;
            TutorialFileLogger.WriteInFile();
            //(new Tables()).prepareForTutorial();
            

            Canvas.SetZIndex(HomeDialog, 9);
            Canvas.SetZIndex(FoggyRect, 10);
            Canvas.SetZIndex(TutorialHomeDialog, 9);
            Canvas.SetZIndex(StartTutorialHomeDialog, 12);
            FoggyRect.Visibility = Visibility.Visible;
            TutorialHomeDialog.Visibility = Visibility.Hidden;
            StartTutorialHomeDialog.Visibility = Visibility.Visible;
            FoggyRect.Opacity = 0.5;
        }

        private void FinishTutorial_ButtonClick(object sender, RoutedEventArgs e)
        {
            DataLists.tutorialOn = false;
            TutorialFileLogger.WriteInFile();
           

            Canvas.SetZIndex(HomeDialog, 14);
            Canvas.SetZIndex(FoggyRect, 10);
            Canvas.SetZIndex(TutorialHomeDialog, 12);
            Canvas.SetZIndex(StartTutorialHomeDialog, 13);
            FoggyRect.Visibility = Visibility.Hidden;
            TutorialHomeDialog.Visibility = Visibility.Hidden;
            StartTutorialHomeDialog.Visibility = Visibility.Hidden;

            
        }

        private void StepTutorial1_ButtonClick(object sender, RoutedEventArgs e)
        {
            Canvas.SetZIndex(HomeDialog, 9);
            Canvas.SetZIndex(FoggyRect, 10);
            Canvas.SetZIndex(TutorialHomeDialog, 12);
            Canvas.SetZIndex(StartTutorialHomeDialog, 9);
            FoggyRect.Visibility = Visibility.Visible;
            TutorialHomeDialog.Visibility = Visibility.Visible;
            StartTutorialHomeDialog.Visibility = Visibility.Hidden;
            FoggyRect.Opacity = 0.5;
        }



        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        public void doThings(string param)
        {
            //btnOK.Background = new SolidColorBrush(Color.FromRgb(32, 64, 128));
            //Title = param;
        }
    }
}
