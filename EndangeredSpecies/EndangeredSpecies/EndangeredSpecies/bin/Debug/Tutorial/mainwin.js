function startIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                intro: "You can use -> or <- keys to navigate thru intros."
              },
              {
                element: document.querySelector('#options'),
                intro: 'An Option is a contract that allows you to sell or buy a stock at a predetermined price within a set time frame. <a href="http://voices.yahoo.com/option-trading-explained-laymans-terms-182594.html?cat=3" target="_blank">Click to know more</a>'
              },
              {
                element: document.querySelectorAll('#warnings')[0],
                intro: "UI validations, errors warnings will be shown here, so watch out for them!",
                position: 'right'
              },
              {
                element: '#code',
                intro: 'Investment Advisors make investment recommendations or conducts securities analysis in return for a fee, whether through direct management of client assets or via written publications. Each advisor has unique codes, which you can select from this list.',
                position: 'left'
              },
			  { 
                intro: "The next step is to select the price."
              },
			  {
                element: '#strike',
                intro: 'The strike or exercise price of an option is the "price" at which the stock will be bought or sold when the option is exercised.',
                position: 'bottom'
              }
            ]
          });

          intro.start();
      }