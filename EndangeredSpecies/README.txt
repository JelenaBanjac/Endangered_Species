Autor: Jelena Banjac SW 16/2013

-------------------------------------------------------------------------------------------
I CheckPoint - Dijalozi za unos, ažuriranje i pregledanje osnovnih skupova podataka
	- Napravljeni su dijalozi za unos/azuriranje/pregled 
	Vrsta(Species)/Tipa Vrsta(SpeciesType)/Etiketa(Tag), bez funkcionalnosti (bez komunikacije sa tabelom).
	- Za sada nije ostvarena povezanost sa tabelom podataka o vrstama zivotinja. Zbog toga, u slucaju azuriranja ili
	pregleda, polja dijaloga nisu popunjena odgovarajucim informacijama.

U slucaju da postoje problemi sa paketom MaterialDesignThemes, uraditi sledece:
	Tools -> NuGet Package Manager -> Package Manager Console -> Ukucati u konzolu: Install-Package MaterialDesignThemes

-------------------------------------------------------------------------------------------
II CheckPoint - DODAVANJA, BRISANJE, IZMENA, TABELARNI PREGLED I SERIJALIZACIJA sva tri entiteta
	- Napravljene su funkcionalnosti za dodavanje/brisanje/izmenu/pregled entiteta Species.
	- Napravljena je serijalizacija Species/SpeciesType/Tag entiteta u foldere na putanji:
		\EndangeredSpecies\EndangeredSpecies\bin\Debug\Species.txt,SpeciesType.txt,Tag.txt
	* Popraviti:
		- Ucitavanje slika za sve dijaloge (add, edit) *** URADJENO ***
		- Popraviti comboboxes za sve opcije *** URADJENO ***
		- Kreirati listu za biranje tagova svake vrste unutar tih dijaloga *** URADJENO ***
		- Korpica za brisanje izabranih tagova (pored + dugmeta).
		- Search za dodavanje tagova
-------------------------------------------------------------------------------------------

III CheckPoint - DRAG & DROP vrsta na karti sveta
	- Napravljene su funkcionalnosti za prebacivanja slika iz liste vrsta na mapu sveta.

U slucaju da postoje problemi sa paketom MaterialDesignThemes, uraditi sledece:
	Tools -> NuGet Package Manager -> Package Manager Console -> Ukucati u konzolu: Install-Package gong-wpf-dragdrop -Version 0.1.4.3 (https://www.nuget.org/packages/gong-wpf-dragdrop/)

	* Popraviti:
		- Dodati kruzic pored svake slike koja predstavlja boju etikete (taga).
		- Zbog slabovidosti korisnika dati pozadinsku boju CardBlock-a iza slike zivotinje na mapi.
		- Popraviti bag sa vracanjem vrsta nazad u praznu listu.

* Za tutorijal instaliran intro.js - Install-Package introjs
* WPF toolkit instaliran zbog nedostatka Autocomplete atributa unutar textBoxa: Install-Package WPFToolkit
 (NE) kao i sledeci paket: Install-Package WPFToolkit.DataVisualization
(NE)Install-Package DotNetProjects.Wpf.Toolkit
(DA) za colopicker Install-Package Extended.Wpf.Toolkit
-------------------------------------------------------------------------------------------

<!-- TAGS field -->
                <materialDesign:DialogHost DialogClosing="Sample1_DialogHost_OnDialogClosing" Margin="8 8 8 0" FontSize="16"
                                        Grid.Column="1" Grid.Row="4" Grid.RowSpan="4" HorizontalAlignment="Left" VerticalAlignment="Center" RenderTransformOrigin="0.5,0.5">
                    <materialDesign:DialogHost.RenderTransform>
                        <TransformGroup>
                            <ScaleTransform/>
                            <SkewTransform AngleY="0.707"/>
                            <RotateTransform/>
                            <TranslateTransform Y="0.988"/>
                        </TransformGroup>
                    </materialDesign:DialogHost.RenderTransform>
                    <materialDesign:DialogHost.DialogContent>
                        <!-- Dialog that opens inside the list of tags (Accept vs. Cancel Dialog) -->
                        <StackPanel Margin="16">
                            <TextBlock>Add a new tag.</TextBlock>
                            <TextBox Margin="0 8 0 0" HorizontalAlignment="Stretch" x:Name="FruitTextBox" />
                            <StackPanel Orientation="Horizontal" HorizontalAlignment="Right" >
                                <Button Style="{StaticResource MaterialDesignFlatButton}"
                                        IsDefault="True"
                                        Margin="0 8 8 0"
                                        Command="{x:Static materialDesign:DialogHost.CloseDialogCommand}">
                                    <Button.CommandParameter>
                                        <system:Boolean>True</system:Boolean>
                                    </Button.CommandParameter>
                                    ACCEPT
                                </Button>
                                <Button Style="{StaticResource MaterialDesignFlatButton}"
                                        IsCancel="True"
                                        Margin="0 8 8 0"
                                        Command="{x:Static materialDesign:DialogHost.CloseDialogCommand}">
                                    <Button.CommandParameter>
                                        <system:Boolean>False</system:Boolean>
                                    </Button.CommandParameter>
                                    CANCEL
                                </Button>
                            </StackPanel>
                        </StackPanel>

                    </materialDesign:DialogHost.DialogContent>

                    <Border BorderThickness="1" BorderBrush="{DynamicResource PrimaryHueMidBrush}"
                                        MinWidth="300" MinHeight="150" ClipToBounds="True">
                        <Grid>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="*" />
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>

                            <ScrollViewer Height="150" Width="300">
                                <ListBox x:Name="FruitListBox">
                                    <ListBoxItem>Apple</ListBoxItem>
                                    <ListBoxItem>Banana</ListBoxItem>
                                    <ListBoxItem>Pear</ListBoxItem>
                                </ListBox>
                            </ScrollViewer>

                            <materialDesign:ColorZone Mode="PrimaryMid" Grid.Row="1" Effect="{DynamicResource MaterialDesignShadowDepth5}">
                                <TextBlock Margin="16">Tags</TextBlock>
                            </materialDesign:ColorZone>
                            <Button Style="{StaticResource MaterialDesignFloatingActionMiniAccentButton}"                                
                                    Command="{x:Static materialDesign:DialogHost.OpenDialogCommand}"
                                    VerticalAlignment="Bottom" HorizontalAlignment="Right" 
                                    Grid.Row="0" Margin="0 0 28 -20">
                                <Viewbox Width="22" Height="22">
                                    <Canvas Width="24" Height="24">
                                        <Path Data="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" Fill="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType=Button}, Path=Foreground}" />
                                    </Canvas>
                                </Viewbox>
                            </Button>
                        </Grid>
                    </Border>

                </materialDesign:DialogHost>
                <!-- (END) TAGS field -->